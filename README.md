# pyLinuxWheel

Is a simple utility to configure Logitech steering wheels for Linux. Currently pyLinuxWheel has the followings
features:

* Range: Allows you to set the wheel range between 40-900 degrees.
* Resistance: set the strength needed to turn the steering wheel.
* Gain: adjust the overall strength of the force feedback effects.
* Combine pedals: use only for old games which can do not work with separate accelerator/brake axis.
* Alternate modes: with this option your wheel can emulate other logitech models. For example, a G29 steering wheel can
    emulate a g27 model.
* Test: you can test force feedback, buttons, pedals, of your steering wheel with pyLinuxWheel.
* Profiles: create, modify, import and export profiles of your steering wheel settings. 
* Auto Adaptive Interface: pyLinuxWheel enable only those options from its interface that are supported for your steering wheel.
* Models supported: Driving Force (EX, RX, Pro, GT), G25, G27, G29, G920, Logitech Racing Wheel USB, WingMan Formula 
(Yellow, GP,Force GP) and MOMO (Force, Racing).
* Automatic installation: pyLinuxWheel can automatic install or update udev rules for your wheel</li>
* AppImage support: pyLinuxWheel don't need installation only download make executable and run it.</li>
* Tested in many distributions: Ubuntu, Debian, OpenSuse, Manjaro, etc</li>
* Free software: pyLinuxWheel is published under GPL3 license</li>
* Multi language: English, French, Spanish and Turkish translations</li>

## Dependencies

If you use pyLinuxWheel as script you have to install python 3 and the following packages: python3-gi, python3-gi-cairo,
 gir1.2-gtk-3.0, python3-pyudev, python3-evdev

Also you can use pyLinuxWheel as AppImage. It has all dependencies included in it.

## Usage

To use pyLinuxWheel you can download and execute the AppImage version or a deb package from https://odintdh.itch.io/pylinuxwheel . Other 
option is download the [source code from gitlab](https://gitlab.com/OdinTdh/pyLinuxWheel/-/archive/master/pyLinuxWheel-master.zip) , unzip and execute the pyLinuxWheel.py
script.

PyLinuxWheel needs sudo permissions to change the values of the Logitech wheel driver. If you don't want to execute
pyLinuxWheel as root or sudo, you have 2 alternatives:

* Let to pyLinuxWheel automatic install the udev rules created by Feral for Logitech Wheels. This is the recommended option.
* Manually install the udev rules for Logitech Wheels. You can use the file 99-logitech-wheel-perms.rules in the rules
directory.

The udev rules file has been originally created by [Feral](https://www.feralinteractive.com/) for its games. A big thanks to give us permission
to use in this program.

## Published in itch.io

PyLinuxWheel is also available in the popular indie store [itch.io](https://itch.io/), so you can download it from the url https://odintdh.itch.io/pylinuxwheel
or using its [app manager itch](https://itch.io/app)

## AppImage

AppImage format permits to package a program with its dependencies. You can download the pyLinuxWheel AppImage that 
I created from [itch.io](https://odintdh.itch.io/pylinuxwheel) but If you want to generate 
the AppImage package in your own computer you can use the buildappimage.sh script.

Before running the pyLinuxWheel AppImage, it is necessary to make it executable. You can make it executable using
your file manager or from the command line with chmod u+x pyLinuxWheel-x86_64.AppImage

## Linux Distributions

AppImage package is created to work in any distribution, but also pyLinuxWheel has native packages for:
* [Gentoo](https://www.gentoo.org/), [ebuild](https://github.com/gripped/Logitech-wheel-ebuilds) created by @gripped
* [MX Linux](https://mxlinux.org/), deb package in Test Repository for MX-19 created by @SwampRabbit



## Supported Logitech racing wheels

* Driving Force (EX, RX, Pro, GT), G25, G27, G29, G920, Logitech Racing Wheel USB, WingMan Formula (Yellow, GP,Force GP) and MOMO (Force, Racing).


pyLinuxWheel adjusts its interface according the features supported by your steering wheel.
Other Logitech models not listed in this section can also work but it could be necessary
to execute pyLinuxWheel with sudo if the steering wheel it does not have a udev rule created in the 99-logitech-wheel-perms.rules 
file. 

## If you have a Logitech Steering Wheel model not supported by pyLinuxWheel

You can execute from the command line the script [wheelDiscover.py](https://gitlab.com/OdinTdh/pyLinuxWheel/blob/master/wheelDiscover.py)
and open a new [issue](https://gitlab.com/OdinTdh/pyLinuxWheel/issues) with the information displayed by wheelDiscover.py

## pyLinuxWheel Translations

pyLinuxWheel is translated into English, French, Spanish and Turkish languages. You can collaborate by
translating into other languages or by improving the current ones.
