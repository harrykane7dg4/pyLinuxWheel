#!/bin/bash

rm -rf linuxdeploy*
rm -rf AppDir
rm -rf _temp_home
rm -f appimagetool-x86_64.AppImage
#download linuxdeploy and conda plugin
wget  https://raw.githubusercontent.com/TheAssassin/linuxdeploy-plugin-conda/master/linuxdeploy-plugin-conda.sh
wget  https://github.com/AppImage/AppImageKit/releases/download/12/appimagetool-x86_64.AppImage
wget  https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-centos6-x86_64.AppImage

# make them executable so that they can be run
chmod +x linuxdeploy*
chmod +x appimagetool-x86_64.AppImage

#create desktop file
cat > io.itch.pyLinuxWheel.desktop << EOF
[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Name=pyLinuxWheel
Exec=pyLinuxWheel.py %u
Icon=pyLinuxWheel
Comment= a steering wheels configuration program for Logitech models
Categories=Utility;
EOF

#download project
BRANCH=compatwheel
wget -c https://gitlab.com/OdinTdh/pyLinuxWheel/-/archive/"$BRANCH"/pyLinuxWheel-"$BRANCH".tar.gz
tar zxvf pyLinuxWheel-"$BRANCH".tar.gz
REPO_ROOT=$(readlink -f pyLinuxWheel-"$BRANCH")
echo "$REPO_ROOT"
cp "$REPO_ROOT"/data/img/icon-64-pyLinuxWheel.png "$REPO_ROOT"/data/img/pyLinuxWheel.png
mv io.itch.pyLinuxWheel.desktop "$REPO_ROOT"/data/
# configure conda plugin
export CONDA_CHANNELS=pkgw-forge
export CONDA_PACKAGES="gtk3;pygobject;pycairo;gettext"
./linuxdeploy-centos6-x86_64.AppImage --appdir AppDir
mkdir -p ./AppDir/usr/share/pyLinuxWheel/
cp -Rf "$REPO_ROOT"/metainfo/ ./AppDir/usr/share/
cp -Rf "$REPO_ROOT"/data/ ./AppDir/usr/share/pyLinuxWheel/
cp -Rf "$REPO_ROOT"/locale/ ./AppDir/usr/share/
cp "$REPO_ROOT"/pyLinuxWheel.py ./AppDir/usr/bin/
./linuxdeploy-centos6-x86_64.AppImage --appdir AppDir  -d "$REPO_ROOT"/data/io.itch.pyLinuxWheel.desktop -i "$REPO_ROOT"/data/img/pyLinuxWheel.png --plugin conda
./appimagetool-x86_64.AppImage -n AppDir
