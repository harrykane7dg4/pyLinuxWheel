��    3      �  G   L      h  b   i  [   �  �   (     �          &     B     H     Y  -  i     �     �     �     �     �     �     �     �               !     &  �   -     �  �   �  �   �	  )   
  ,   F
  *   s
     �
     �
  !   �
     �
     �
     �
     �
  
   �
          
               1     7  �   O  9   �          )     I     h  0   �  H  �  v     c   y  �   �     �     �     �  	   �            `  .  	   �     �     �  ,   �     �     �  
   
          "     7     E     L  �   Z      >  �   _  �     3   �  2   �  5   0     f     l  /   x     �     �     �     �     �  	   �     �     �     �  
     #   %  �   I  #   
     .     ;     V     p  5   �        &      )           !             ,         (   .                                $   /   1   *       '                  0                                   #      %   "              3                           
          2   +       -   	              You are executing a new version of pyLinuxWheel. Do you want to update Logitech wheel udev rules? <a href="https://www.jugandoenlinux.com"title="jugandoenlinux">www.jugandoenlinux.com</a>   <b>Credits</b>

Alberto Vicente aka <i>Odin</i>, lead developer
CansecoGPC, logo designer
Leillo1975, SQA
Francisco aka <i>P_Vader</i>, SQA
Krafting, french translation
Hüseyin Fahri Uzun, turkish translation <b>Preferences</b> <b>Special thanks to</b> <b>pyLinuxWheel</b>
v0.6.1  About Advanced options Alternate Modes An utility to configure Logitech steering
wheels  for Linux under the <a href="https://www.gnu.org/licenses/gpl-3.0.html" title="GPLv3 License">GPLv3 License</a>.
Go to <a href="https://gitlab.com/OdinTdh/pyLinuxWheel"                 title="Our website">pyLinuxWheel</a> website
for more information. Author Brake pedal Button clicked Check udev rules at start: Clutch pedal Combine Pedals Description Export Export profile Force Feedback Gain Import It is necessary an Internet connection to update logitech wheel udev rules. Do you want to update logitech wheel udev rules to execute pyLinuxWheel as normal user ? Load last saved value: Logitech wheel udev rules are not installed. You can not change your wheel driver without root privileges and with Logitech wheel udev rules non installed Logitech wheel udev rules are not update. You can not change your wheel driver without root privileges and with logitech wheel udev rules non installed Logitech wheel udev rules are not updated Logitech wheel udev rules has been installed Logitech wheel udev rules has been updated Name New Profile Please choose a profile to import Preferences Profile Profile Editor Range Resistance Tags Test Throttle pedal Update udev rules: Wheel Wheel Editor Properties You are executing pyLinuxWheel without root privileges. Do you want to install Logitech wheel udev rules to execute  pyLinuxWheel as normal user ? adjust the overall strength of the force feedback effects pyLinuxWheel pyLinuxWheel Post-Configuration pyLinuxWheel Pre-Configuration pyLinuxWheel Udev Configuration set the strength need to turn the steering wheel Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-03 14:10+0300
Last-Translator: Hüseyin Fahri Uzun <mail@fahriuzun.com>
Language-Team: 
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Plural-Forms: nplurals=2; plural=(n != 1);
  pyLinuxWheel yeni versiyonu çalıştırıyorsunuz. Logitech direksiyon udev kurallarını güncelleme ister misiniz? <a href=“https://www.jugandoenlinux.com”title=“jugandoenlinux”>www.jugandoenlinux.com</a>   <b>Hakkında</b>

Alberto Vicente aka <i>Odin</i>, baş geliştirici
CansecoGPC, logo tasarımcısı
Leillo1975, SQA
Francisco aka <i>P_Vader</i>, SQA
Krafting, fransızca çeviriHüseyin Fahri Uzun, türkçe çeviri <b>Ayarlar</b> <b>Özel teşekkürler</b> <b>pyLinuxWheel</b>
v0.6.1  Hakkında İleri düzey ayarlar Alternatif Modlar Logitech direksiyonlarını ayarlayan
Linux için yazılmış ve lisans olarak <a href=“https://www.gnu.org/licenses/gpl-3.0.html” title=“GPLv3 License”>GPLv3 License</a> olan bir araç.
Daha fazla bilgi için websitesine <a href=“https://gitlab.com/OdinTdh/pyLinuxWheel”                 title=“Our website”>pyLinuxWheel</a> 
bakınız. Yayıncı Fren pedalı Tuşa basıldı Başlatırken udev kurallarını kontrol et: Debriyaj pedalı Pedalları Birleştir Açıklama Dışa Aktar Profili dışa aktar Geri Bildirim Seviye İçeri Aktar Logitech direksiyonu udev kurallarını güncellemek için internet bağlantısı gerekir. pyLinuxWheel’in normal kullanıcı olarak çalışabilmesi için logitech direksiyon udev kuralları güncellenmesini istiyor musunuz? Son kayıtlı değerleri yükle: Logitech direksiyonu udev kuralları yüklü değil. Logitech direksiyonu udev kuralları yüklü değilken ve root erişimi yok iken direksiyon sürücüsünü değiştiremezsiniz Logitech direksiyonu udev kuralları güncel değil. Logitech direksiyonu udev kuralları yüklü değilken ve root erişimi yok iken direksiyon sürücüsünü değiştiremezsiniz Logitech direksiyon udev kuralları güncellenemedi Logitech direksiyon udev kuralları yüklenmiştir Logitech direksiyon udev kuralları güncellenmiştir İsim Yeni Profil Lütfen içe aktarım için bir profil seçiniz Seçenekler Profil Profil Düzenleyicisi Aralık Direnç Etiketler Test Gaz pedalı Udev kurallarını güncelle: Direksiyon Direksiyon Düzenleyicisi Ayarları Root erişimi olmadan pyLinuxWheel’i çalıştırıyorsunuz. Logitech direksiyon udev kurallarını pyLinuxWheel’in normal kullanıcı olarak çalışması için yüklemek istiyor musunuz? geri bildirim şiddetini ayarlayın pyLinuxWheel pyLinuxWheel Ayar-Sonrası pyLinuxWheel Ayar-Öncesi pyLinuxWheel Udev Ayarı direksiyonu çevirmek için gereken gücü ayarlayın 