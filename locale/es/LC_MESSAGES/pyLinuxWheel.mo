��    3      �  G   L      h  b   i  [   �  �   (     �          &     B     H     Y  -  i     �     �     �     �     �     �     �     �               !     &  �   -     �  �   �  �   �	  )   
  ,   F
  *   s
     �
     �
  !   �
     �
     �
     �
     �
  
   �
          
               1     7  �   O  9   �          )     I     h  0   �  V  �  q     [   �  �   �     �     �       	   !     +     >  =  Q     �     �     �      �     �     �     �     �     �               "  �   +      �  �     �   �  >   j  9   �  ;   �          &     3     Q     ^     e     x     ~  	   �     �  
   �     �     �  !   �  �   �  2   �     �      �           "  3   C        &      )           !             ,         (   .                                $   /   1   *       '                  0                                   #      %   "              3                           
          2   +       -   	              You are executing a new version of pyLinuxWheel. Do you want to update Logitech wheel udev rules? <a href="https://www.jugandoenlinux.com"title="jugandoenlinux">www.jugandoenlinux.com</a>   <b>Credits</b>

Alberto Vicente aka <i>Odin</i>, lead developer
CansecoGPC, logo designer
Leillo1975, SQA
Francisco aka <i>P_Vader</i>, SQA
Krafting, french translation
Hüseyin Fahri Uzun, turkish translation <b>Preferences</b> <b>Special thanks to</b> <b>pyLinuxWheel</b>
v0.6.1  About Advanced options Alternate Modes An utility to configure Logitech steering
wheels  for Linux under the <a href="https://www.gnu.org/licenses/gpl-3.0.html" title="GPLv3 License">GPLv3 License</a>.
Go to <a href="https://gitlab.com/OdinTdh/pyLinuxWheel"                 title="Our website">pyLinuxWheel</a> website
for more information. Author Brake pedal Button clicked Check udev rules at start: Clutch pedal Combine Pedals Description Export Export profile Force Feedback Gain Import It is necessary an Internet connection to update logitech wheel udev rules. Do you want to update logitech wheel udev rules to execute pyLinuxWheel as normal user ? Load last saved value: Logitech wheel udev rules are not installed. You can not change your wheel driver without root privileges and with Logitech wheel udev rules non installed Logitech wheel udev rules are not update. You can not change your wheel driver without root privileges and with logitech wheel udev rules non installed Logitech wheel udev rules are not updated Logitech wheel udev rules has been installed Logitech wheel udev rules has been updated Name New Profile Please choose a profile to import Preferences Profile Profile Editor Range Resistance Tags Test Throttle pedal Update udev rules: Wheel Wheel Editor Properties You are executing pyLinuxWheel without root privileges. Do you want to install Logitech wheel udev rules to execute  pyLinuxWheel as normal user ? adjust the overall strength of the force feedback effects pyLinuxWheel pyLinuxWheel Post-Configuration pyLinuxWheel Pre-Configuration pyLinuxWheel Udev Configuration set the strength need to turn the steering wheel Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-07-24 16:17+0200
Last-Translator: odin <odintdh@gmail.com>
Language-Team: Spanish
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
 Está ejecutando una nueva versión de pyLinuxWheel. ¿Desea actualizar las reglas udev para su volante Logitech? <a href="https://www.jugandoenlinux.com"title="jugandoenlinux">www.jugandoenlinux.com</a>   <b>Créditos</b>

Alberto Vicente alias <i>Odin</i>, desarrollador principal
CansecoGPC, diseñador del logo del programa
Leillo1975, SQA
Francisco alias <i>P_Vader</i>, SQA
Krafting, traducción al francés 
Hüseyin Fahri Uzun, traducción al turco <b>Preferencias</b> <b>Agradecimientos a</b> <b>pyLinuxWheel</b>
v0.6.1 Acerca de Opciones avanzadas Modos alternativos Una utilidad para configurar volantes 
Logitech  para Linux publicada con <a href="https://www.gnu.org/licenses/gpl-3.0.html" title="GPLv3 License">licencia GPLv3</a>.  
Visite el sitio web <a href="https://gitlab.com/OdinTdh/pyLinuxWheel"                 title="Our website">pyLinuxWheel</a> 
para más información. Autor Freno Botón pulsado Comprobar reglas udev al inicio: Embrague Combinar pedales Descripción Exportar Exportar perfil Force Feedback Aumento Importar Se requiere de conexión a Internet para actualizar las reglas udev de su volante Logitech. ¿Desea actualizar las reglas udev de su volante Logitech para ejecutar pyLinuxWheel sin permisos de root? Recargar último valor guardado: Las reglas udev para volantes Logitech no están instaladas. No puede cambiar los valores de su volante Logitech sin permisos de root si las reglas udev no están instaladas. Las reglas udev de su volante Logitech no están actualizadas. No puede cambiar los valores de su volante sin permisos de root si las reglas udev no están instaladas. No se han actualizado las reglas udev para su volante Logitech Se han instalado las reglas udev para su volante Logitech Se han actualizado las reglas udev para su volante Logitech Nombre Nuevo Perfil Elija un perfil para importar Preferencias Perfil Editor de Perfiles Rango Resistencia Etiquetas Test Acelerador Actualizar reglas udev: Volante Editor de Propiedades del Volante Está ejecutando pyLinuxWheel sin permisos de root. ¿ Desea instalar las reglas udev para su volante Logitech que le permitirá ejecutar pyLinuxWheel sin necesidad de permisos de root ? ajusta la fuerza de los efectos del force feedback pyLinuxWheel pyLinuxWheel Post-Configuración pyLinuxWheel Pre-Configuración pyLinuxWheel Configuración Udev configura la fuerza necesaria para girar el volante 