��            )         �  b   �  [     �   p     B     U     n     �     �     �  -  �     �     �     	  �        �  �   �  �   o  )     ,   1  *   ^     �     �     �     �     �  �   �     _	     l	     �	     �	  U  �	  x   !  [   �  �   �     �      �     
  	   %     /     A  1  S  (   �     �     �  �   �  )   �  �   �  �   �  ;     9   [  ;   �     �     �     �       $     �   0     �     �          -                
                                                                                                                 	        You are executing a new version of pyLinuxWheel. Do you want to update Logitech wheel udev rules? <a href="https://www.jugandoenlinux.com"title="jugandoenlinux">www.jugandoenlinux.com</a>   <b>Credits</b>

Alberto Vicente aka <i>Odin</i>, lead developer
CansecoGPC, logo designer
Leillo1975, SQA
Francisco aka <i>P_Vader</i>, SQA
Krafting, french translation
Hüseyin Fahri Uzun, turkish translation <b>Preferences</b> <b>Special thanks to</b> <b>pyLinuxWheel</b>
v0.6.1  About Advanced options Alternate Modes An utility to configure Logitech steering
wheels  for Linux under the <a href="https://www.gnu.org/licenses/gpl-3.0.html" title="GPLv3 License">GPLv3 License</a>.
Go to <a href="https://gitlab.com/OdinTdh/pyLinuxWheel"                 title="Our website">pyLinuxWheel</a> website
for more information. Check udev rules at start: Combine Pedals Force Feedback It is necessary an Internet connection to update logitech wheel udev rules. Do you want to update logitech wheel udev rules to execute pyLinuxWheel as normal user ? Load last saved value: Logitech wheel udev rules are not installed. You can not change your wheel driver without root privileges and with Logitech wheel udev rules non installed Logitech wheel udev rules are not update. You can not change your wheel driver without root privileges and with logitech wheel udev rules non installed Logitech wheel udev rules are not updated Logitech wheel udev rules has been installed Logitech wheel udev rules has been updated Preferences Range Update udev rules: Wheel Wheel Editor Properties You are executing pyLinuxWheel without root privileges. Do you want to install Logitech wheel udev rules to execute  pyLinuxWheel as normal user ? pyLinuxWheel pyLinuxWheel Post-Configuration pyLinuxWheel Pre-Configuration pyLinuxWheel Udev Configuration Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-07-24 16:17+0200
Last-Translator: odin <odintdh@gmail.com>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
 Vous éxécutez une nouvelle version de pyLinuxWheel. Voulez vous installer les règles udev pour les Volants Logitech ? <a href="https://www.jugandoenlinux.com"title="jugandoenlinux">www.jugandoenlinux.com</a>   <b>Crédits</b>

Alberto Vicente alias <i>Odin</i>, développeur principal
CansecoGPC, logo designer 
Leillo1975, SQA
Francisco alias <i>P_Vader</i>, SQA
Krafting, traduction françaiseHüseyin Fahri Uzun, traduction turc <b>Préférences</b> <b>Remerciement spéciaux à</b> <b>pyLinuxWheel</b>
v0.6.1 À propos Options avancées Modes alternatifs Un utilitaire pour configurer les volants Logitech 
pour Linux sous la <a href="https://www.gnu.org/licenses/gpl-3.0.html" title="GPLv3 License">license GPLv3</a>.  
Allez sur <a href="https://gitlab.com/OdinTdh/pyLinuxWheel"                 title="Our website">pyLinuxWheel</a> 
pour plus d'informations. Verifier les règles udev au démarrage: Combiner les pédales Force Feedback Il est néccéssaire d'avoir une connexion internet pour mettre à jour les règles udev du volant Logitech. Voulez vous mettre à jour les règles udev du volant logitech pour exécuter pyLinuxWheel en tant qu'utilisateur normal ? Charger la dernière valeur sauvegardée: Les règles udev du volant logitech ne sont pas installées. Vous ne pouvez pas changer votre driver de volant sans privilèges root et sans les règles udev. Les règles udev du volant logitech ne sont pas à jour. Vous ne pouvez pas changer votre driver de volant sans privilèges root et sans les règles udev. Les règles udev du volant Logitech ont été mises à jour Les règles udev du volant Logitech ont été installées Les règles udev du volant Logitech ont été mises à jour Préférences Portée Actualiser les règles udev: Volant Propriétés de l'éditeur de Volant Vous éxécutez pyLinuxWheel sans privilèges root. Voulez vous installer les règles udev pour les Volants Logitech pour exécuter pyLinuxWheel en tant qu'utilisateur normal ? pyLinuxWheel pyLinuxWheel Post-Configuration pyLinuxWheel Pre-Configuration pyLinuxWheel Configuration Udev 